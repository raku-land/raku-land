unit module Local::Routes::Autocomplete;

use Cro::HTTP::Router;
use Local::DB;

get -> 'autocomplete', :$q {
    content 'application/json', db.query( q:to/SQL/, $q ).arrays.flat.Array;
        WITH distinct_names AS (
            SELECT name, MAX(stars) stars FROM distinct_dists GROUP BY name
        ) SELECT name
            FROM distinct_names
           WHERE name LIKE $1 || '%' OR name LIKE '%::' || $1 || '%'
        ORDER BY name = $1::citext DESC, stars DESC NULLS LAST, name
           LIMIT 10
    SQL
}
