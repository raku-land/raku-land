// TODO Provide some kind of feedback on success?
document.querySelector('#long-name').onclick = e =>
    navigator.clipboard.writeText(e.currentTarget.innerText);

// Add "#" links under each markdown heading.
for (const header of document.querySelectorAll('[id^=rl-]')) {
    const a = document.createElement('a');
    a.href = '#' + header.id.slice(3);
    a.setAttribute('aria-hidden', 'true');
    a.textContent = '#';

    header.append(a);
}

// Scroll to headers on hash change.
onhashchange = () => document.querySelector(
    "#rl-" + location.hash.slice(1).toLowerCase())?.scrollIntoView(true);

// In case we're here before DOM ready ensure it runs then.
window.addEventListener('DOMContentLoaded', onhashchange);

// In case we're here after DOM ready ensure it runs now.
onhashchange();
